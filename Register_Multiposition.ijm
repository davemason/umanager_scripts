// @File(label = "Input directory", style = "directory") input
// @File(label = "Output directory", style = "directory") output
// @String(label = "File suffix", value = ".tif") suffix

/*
 * Macro to landmark-register the results of a 24 well plate acquisition
 *
 *
 *
 * 												Dave Mason [dnmason@liv.ac.uk] August 2017
 * 												Centre for Cell Imaging [http://cci.liv.ac.uk]
 * 												University of Liverpool
 * 												
 *										 		Provided under a CCBY 4.0 Licence
 *										 		[https://creativecommons.org/licenses/by/4.0/]
 */

processFolder(input);


// function to scan folders/subfolders/files to find files with correct suffix
function processFolder(input) {
	list = getFileList(input);
	list = Array.sort(list)
	for (i = 0; i < list.length; i++) {
		if(endsWith(list[i], suffix))
			processFile(input, output, list[i]);
	}
}

function processFile(input, output, file) {
print("Processing: " + input + File.separator+ file);


open(input + File.separator+ file);
title=getTitle();
//-- Get calibration and dimensions
getDimensions(w, h, c, s, f);
getPixelSize(u, pW, pH);
//t=Stack.getFrameInterval();

run("Descriptor-based series registration (2d/3d + t)", "series_of_images="+title+" brightness_of=[Advanced ...] approximate_size=[Advanced ...] type_of_detections=[Maxima only] subpixel_localization=[3-dimensional quadratic fit] transformation_model=[Translation (2d)] images_are_roughly_aligned number_of_neighbors=3 redundancy=1 significance=3 allowed_error_for_ransac=5 global_optimization=[Consecutive matching of images (no global optimization)] range=5 choose_registration_channel=1 image=[Fuse and display] detection_sigma=3 threshold=0.02");
run("Next Slice [>]");
run("Grays");
resetMinAndMax();
//-- Apply calibration back to the image
run("Properties...", "channels="+c+" slices="+s+" frames="+f+" unit="+u+" pixel_width="+pW+" pixel_height="+pH+" voxel_depth=1 frame=[120 sec]");
saveAs("Tiff", output+File.separator+"registered_"+title);
close("*");
	
print("Saved to: " + output+File.separator+"registered_"+title);
}


